describe("Round", function() {
  var round;

  beforeEach(function() {
    round = new Round();
  });

  describe("defaults", function() {
    it("has two rolls, which are both null", function() {
      expect(round.rollOne).toBe(null);
      expect(round.rollTwo).toBe(null);
    });

    it("has an empty result", function() {
      expect(round.result).toEqual([]);
    });

    it("is in progress", function() {
      expect(round.isInProgress).toBe(true);
    });
  });

  describe("acceptPins", function() {
    it("only accepts numbers between 0 and 10", function() {
      expect(function() {round.acceptPins("one");}).toThrow("Invalid pin entry");
      expect(function() {round.acceptPins(15);}).toThrow("Invalid pin entry");
    });

    it("accepts entries and stores them in the correct variables", function() {
      round.acceptPins(8);
      round.acceptPins(1);
      expect(round.rollOne).toEqual(8);
      expect(round.rollTwo).toEqual(1);
    });

    it("does not overwrite existing rolls", function() {
      round.acceptPins(8);
      round.acceptPins(1);
      round.acceptPins(5);
      expect(round.rollOne).toEqual(8);
      expect(round.rollTwo).not.toEqual(5);
    });

    it("only accepts a second roll if the round total won't be exceeded", function() {
      round.acceptPins(8);
      expect(function() {round.acceptPins(3);}).toThrow("Invalid pin entry");
    });
  });

  describe("acceptFinalPins", function() {
    it("only accepts numbers between 0 and 10", function() {
      expect(function() {round.acceptFinalPins("one");}).toThrow("Invalid pin entry");
      expect(function() {round.acceptFinalPins(15);}).toThrow("Invalid pin entry");
    });

    it("is still in progress if the sum of the first two numbers >= 10", function() {
      round.acceptFinalPins(5);
      round.acceptFinalPins(5);
      expect(round.isInProgress).toBe(true);
    });
  });

  describe("isFull", function() {
    it("returns the opposite of whether the game is in progress", function() {
      expect(round.isFull()).toBe(false);
    });
  });
});
